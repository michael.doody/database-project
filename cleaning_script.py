#iowa liquor sales cleaning script
import pandas as pd

# read csv file
original = pd.read_csv('Iowa_Liquor_Sales.csv')

# counties have a mix of upper and lower case values
# better to be consistent
original['County'] = original['County'].str.lower()

# create store data
store = original[['Store Number', 'Store Name', 'Address',
         'City', 'Zip Code', 'Store Location', 'County Number']]

store = store.groupby('Store Number').agg({'Store Name': 'first',
                                  'Address': 'first',
                                  'City': 'first',
                                  'Zip Code': 'first',
                                  'Store Location': 'first',
                                  'County Number': 'first'}).reset_index()

store = store.rename(columns={'Store Name': 'store_name',
                              'Store Number': 'store_id',
                              'Address': 'address',
                              'City': 'city',
                              'Zip Code': 'zip_code',
                              'Store Location': 'store_location',
                              'County Number': 'county_id'})

store['store_location'] = store['store_location'].str.extract(r"\((.*?)\)")

store['latitude'] = store['store_location'].str.extract('(.*,)')
store['latitude'] = store['latitude'].str.replace(',', '')

store['longitude'] = store['store_location'].str.extract('(,.*)')
store['longitude'] = store['longitude'].str.replace(', ', '')

store['address'] = store['address'].str.replace('\n', '')

#create county data
county = original[['County Number', 'County']]
county = county.groupby('County Number').first().reset_index()

county = county.rename(columns={'County Number': 'county_id',
                                'County': 'county_name'})

county['county_id'] = county['county_id'].astype(int)

# create category data
category = original[['Category', 'Category Name']]
category = category.groupby('Category').first().reset_index()

category = category.rename(columns={'Category': 'category_id',
                                    'Category Name': 'category_name'})

category['category_id'] = category['category_id'].astype(int)

# create vendor data
vendor = original[['Vendor Number', 'Vendor Name']]
vendor = vendor.groupby('Vendor Number').first().reset_index()

vendor = vendor.rename(columns={'Vendor Number': 'vendor_id',
                                'Vendor Name': 'vendor_name'})

vendor['vendor_id'] = vendor['vendor_id'].astype(int)

# create item data
item = original[['Item Number', 'Item Description']]
item = item.groupby('Item Number').first().reset_index()

item = item.rename(columns={'Item Number': 'item_id',
                            'Item Description': 'item_desc'})

# clean original for sale_data
sale_data = original[['Invoice/Item Number', 'Date', 'Store Number',
                     'County Number', 'Category', 'Vendor Number',
                     'Item Number', 'Pack', 'Bottle Volume (ml)',
                     'State Bottle Cost', 'State Bottle Retail',
                     'Bottles Sold', 'Sale (Dollars)', 'Volume Sold (Liters)',
                     'Volume Sold (Gallons)']]

sale_data['State Bottle Cost'] = sale_data['State Bottle Cost'].str.replace('[\$,)]','').astype(float)
sale_data['State Bottle Retail'] = sale_data['State Bottle Retail'].str.replace('[\$,)]','').astype(float)
sale_data['Sale (Dollars)'] = sale_data['Sale (Dollars)'].str.replace('[\$,)]','').astype(float)

sale_data = sale_data.rename(columns={'Invoice/Item Number': 'invoice',
                                      'Date': 'date',
                                      'Store Number': 'store_id',
                                      'County Number': 'county_id',
                                      'Category': 'category_id',
                                      'Vendor Number': 'vendor_id',
                                      'Item Number': 'item_id',
                                      'Pack': 'pack',
                                      'Bottle Volume (ml)': 'bottle_volume',
                                      'State Bottle Cost': 'state_bottle_cost',
                                      'State Bottle Retail': 'state_bottle_retail',
                                      'Bottles Sold': 'bottles_sold',
                                      'Sale (Dollars)': 'sales',
                                      'Volume Sold (Liters)': 'volumn_sold_liters',
                                      'Volume Sold (Gallons)': 'volume_sold_gallons'})
# Fix null counties in sale_data
sale_data = sale_data.merge(store, on='store_id')

sale_data = sale_data.drop(['county_id_x', 'latitude', 'longitude', 'store_location',
               'zip_code', 'address', 'store_name', 'city'], axis=1)

sale_data = sale_data.rename(columns={'county_id_y': 'county_id'})

# create csv's for all the different datasets
store.to_csv('store.csv', index=False)
county.to_csv('county.csv', index=False)
category.to_csv('category.csv', index=False)
vendor.to_csv('vendor.csv', index=False)
item.to_csv('item.csv', index=False)
sale_data.to_csv('sale_data.csv', index=False)