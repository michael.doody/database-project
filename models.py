import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import pandas as pd
from eralchemy import render_er

from sqlalchemy import (MetaData, Table, Column, Integer, String, ForeignKey, Date, Float)    
metadata = MetaData()

# create your own model ....
store = Table('store', metadata,
    Column('store_id', Integer(), primary_key=True),
    Column('store_name', String(50)),
    Column('address', String(50)),
    Column('city', String(50)),
    Column('zip_code', String(50)),
    Column('store_location', String(50)),
    Column('latitude', Float()),
    Column('longitude', Float()),
    Column('county_id', ForeignKey('county.county_id'))
)    
category = Table('category', metadata,
    Column('category_id', Integer(), primary_key=True),
    Column('category_name', String(50)),
)
item = Table('item', metadata,
    Column('item_id', Integer(), primary_key=True),
    Column('item_desc', String(50)),
)
vendor = Table('vendor', metadata,
    Column('vendor_id', Integer(), primary_key=True),
    Column('vendor_name', String(50)),
)
county = Table('county', metadata,
    Column('county_id', Integer(), primary_key=True),
    Column('county_name', String(50)),
)

sale_data = Table('sale_data', metadata,
    Column('invoice', String(250), primary_key=True),
    Column('category_id', ForeignKey('category.category_id')),
    Column('store_id', ForeignKey('store.store_id')),
    Column('item_id', ForeignKey('item.item_id')),
    Column('vendor_id', ForeignKey('vendor.vendor_id')),
    Column('county_id', ForeignKey('county.county_id')),
    Column('date', Date()),
    Column('pack', Integer()),
    Column('bottle_volume', Integer()),
    Column('state_bottle_cost', Float()),
    Column('state_bottle_retail', Float()),
    Column('bottles_sold', Integer()),
    Column('sales', Float()),
    Column('volume_sold_liters', Float()),
    Column('volume_sold_gallons', Float()),
)


# add your own table ....

# Show ER model from here
filename = 'iowa_liquor_er_diagram.png'
render_er(metadata, filename)
imgplot = plt.imshow(mpimg.imread(filename))
plt.rcParams["figure.figsize"] = (15,10)
plt.show()