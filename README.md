## Database Course Project thingy
---
This is a repo for cleaning the Iowa Liquor Sales iowa data which can be found [HERE](https://data.iowa.gov/Economy/Iowa-Liquor-Sales/m3tr-qhgy/data)
I won't comit that output to git because it is massive and not worth it. FILTER IT FOR THE LAST 3 YEARS!!!!!!!


# Installation 
To begin install [MiniConda](https://conda.io/miniconda.html) for your respective OS.

Chose the python 3.6 this is the newest version of python. If you want 
you can install Conda. If you're walking on ice you might as well dance.

Once that is installed run the following:

```
# Clone the repo (you will need git for this if you dont want to do that then read below this step)
git clone git@gitlab.com:michael.doody/database-project.git

or you can just download it from the repo by clicking the download button
which is probably best for our purposes

# Create virtual env
conda create -n env_name python

# Start virtual environment

# If you have a mac run
source activate env_name

# For windows run 
activate env_name 

# Install dependencies
conda install pandas

# Save iowa liquor csv to folder 
save the iowa liquor csv output file to this repo folder

# Run the cleaning script I wrote which is in this repo
python cleaning_script.py
```

The script will clean the iowa liquor csv you downloaded from the site and 
output csv files for all the "tables" we will be using for tableau. It takes about
30 seconds to run. That's slow but not too bad. No need for any optimization. 

Yay we can get to work now!!!!

## Using Tableau

Open Tableau and select from text file source and then select the `sale_data.csv` file
created from the steps taken above. This should open the folder with all the csv 
files we will be using.

```
sale_data.csv
county.csv
category.csv
vendor.csv
store.csv
item.csv
```


There are some null foreign key values in the `sale_data.csv` file. In order to 
get all the data we will have to do a left/right join on the `sale_data.csv` file.

I timed the extract and it takes about 2 minutes to extract. Once we extract the data for
Tableau to read then everything should be good to go and the calculations and stuff won't 
take too long.  

## Database Stuff ER Diagram

![alt text](iowa_liquor_er_diagram.png "ER Diagram")

## Web Scrape and String Matching

The `web_scrape.py` file was used to scrape the wikipedia page to get the html table
containing all the [Iowa Cities](https://en.wikipedia.org/wiki/List_of_cities_in_Iowa) and their proper spelling. Then I use a string matching
package to match the proper cities with their improperly spelled cities and update the 
`store.csv` file to make it all better. Now Tableau will recognize the city and we can 
get more accurate findings. There are other packages you will need to install so I added 
the `requirements.txt` file for all the packages I use. I'll just save all the files 
on a flash drive. 
