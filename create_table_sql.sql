/*
Create Table statements to create the sql tables if you want
Just have to import the csv files made from the cleaning_script.py 
and the web_scrape.py
*/

CREATE TABLE category (
        category_id INTEGER NOT NULL,
        category_name VARCHAR(50) NOT NULL,
        PRIMARY KEY (category_id)
)

CREATE TABLE item (
        item_id INTEGER NOT NULL,
        item_desc VARCHAR(50) NOT NULL,
        PRIMARY KEY (item_id)
)

CREATE TABLE vendor (
        vendor_id INTEGER NOT NULL,
        vendor_name VARCHAR(50) NOT NULL,
        PRIMARY KEY (vendor_id)
)

CREATE TABLE county (
        county_id INTEGER NOT NULL,
        county_name VARCHAR(50) NOT NULL,
        PRIMARY KEY (county_id)
)

CREATE TABLE store (
        store_id INTEGER NOT NULL,
        store_name VARCHAR(50) NOT NULL,
        address VARCHAR(50),
        city VARCHAR(50),
        zip_code VARCHAR(50),
        store_location VARCHAR(50),
        latitude FLOAT,
        longitude FLOAT,
        county_id INTEGER,
        PRIMARY KEY (store_id),
        FOREIGN KEY(county_id) REFERENCES county (county_id)
)

CREATE TABLE sale_data (
        invoice VARCHAR(250) NOT NULL,
        category_id INTEGER,
        store_id INTEGER,
        item_id INTEGER,
        vendor_id INTEGER,
        county_id INTEGER,
        date DATE,
        pack INTEGER,
        bottle_volume INTEGER,
        state_bottle_cost FLOAT,
        state_bottle_retail FLOAT,
        bottles_sold INTEGER,
        sales FLOAT,
        volume_sold_liters FLOAT,
        volume_sold_gallons FLOAT,
        PRIMARY KEY (invoice),
        FOREIGN KEY(category_id) REFERENCES category (category_id),
        FOREIGN KEY(store_id) REFERENCES store (store_id),
        FOREIGN KEY(item_id) REFERENCES item (item_id),
        FOREIGN KEY(vendor_id) REFERENCES vendor (vendor_id),
        FOREIGN KEY(county_id) REFERENCES county (county_id)
)