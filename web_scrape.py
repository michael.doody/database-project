#Used to scrape wikipedia for proper city spelling
from bs4 import BeautifulSoup
import urllib.request
import pandas as pd
from fuzzywuzzy import fuzz
from fuzzywuzzy import process

wiki = "https://en.wikipedia.org/wiki/List_of_cities_in_Iowa"
header = {'User-Agent': 'Mozilla/5.0'} #Needed to prevent 403 error on Wikipedia
req = urllib.request.Request(wiki,headers=header)
page = urllib.request.urlopen(req)
soup = BeautifulSoup(page, "html5lib")
 

table = soup.find_all("table")[1]

iowa_cities = str(table)
iowa_cities = pd.read_html(iowa_cities, header=0)[0]
iowa_cities = list(iowa_cities['Name'][1:])

store = pd.read_csv('store.csv')
store['city'] = store['city'].str.lower()
store_cities = list(store.city.unique())

change_dict = {}
for city in store_cities:
    match = process.extractOne(city, iowa_cities)[0]
    change_dict[city] = match

store = store.replace({"city": change_dict})
store.to_csv('store.csv', index=False)
